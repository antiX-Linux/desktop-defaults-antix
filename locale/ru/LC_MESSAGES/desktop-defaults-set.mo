��          t      �                      +     8  /   E     u     �     �     �     �  �  �     �     �  !   �  -     j   >  Q   �     �  #        0     E               	                 
                            Audio Player Email Client File Manager Image Viewer No localized name found, using the original one Set Default Applications Terminal Text Editor Video Player Web Browser Project-Id-Version: desktop-defaults-set
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:13+0000
Last-Translator: Andrei Stepanov, 2022
Language-Team: Russian (https://www.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Аудиоплеер Почтовый клиент Файловый Менеджер Просмотрщик изображений Локализованное имя не найдено, используется оригинальное Выбор приложений, используемых по умолчанию Терминал Текстовый редактор Видеоплеер Браузер 