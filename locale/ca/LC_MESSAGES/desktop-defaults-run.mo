��          4      L       `   %   a   X   �   ~  �   )   _  W   �                    Need to specify an application to run The program being started is myself. \n You will need to select a program other than me. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:12+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2021
Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Cal especificar una aplicació a executar El programa que s'executa és aquest mateix.\n Cal triar un programa diferent d'aquest. 