��          t      �                      +     8  /   E     u     �     �     �     �  �  �     @     R     j     |  3   �  %   �  	   �     �                         	                 
                            Audio Player Email Client File Manager Image Viewer No localized name found, using the original one Set Default Applications Terminal Text Editor Video Player Web Browser Project-Id-Version: desktop-defaults-set
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:13+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2022
Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Reproductor de so Client de c/electrònic Gestor de fitxers Visualitzador d'imatges No he trobat el nom localitzat, s'usarà l'original Defineix les aplicacions per omissió Terminal  Editor de text Reproductor de Vídeo Navegador web 