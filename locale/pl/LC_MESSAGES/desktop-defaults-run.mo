��          4      L       `   %   a   X   �     �   ,   �  M   $                    Need to specify an application to run The program being started is myself. \n You will need to select a program other than me. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:12+0000
Last-Translator: MX Linux Polska <mxlinuxpl@gmail.com>, 2021
Language-Team: Polish (https://www.transifex.com/anticapitalista/teams/10162/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 Musisz określić aplikację do uruchomienia Uruchamiany program to ja. \n Będziesz musiał wybrać inny program niż ja. 