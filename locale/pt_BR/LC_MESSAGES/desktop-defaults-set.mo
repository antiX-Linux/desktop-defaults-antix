��          t      �                      +     8  /   E     u     �     �     �     �  �  �     �     �     �     �  r   �     W     v     �     �     �               	                 
                            Audio Player Email Client File Manager Image Viewer No localized name found, using the original one Set Default Applications Terminal Text Editor Video Player Web Browser Project-Id-Version: desktop-defaults-set
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:13+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2022
Language-Team: Portuguese (Brazil) (https://www.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Reprodutor de Áudio Cliente de E-mail Gerenciador de Arquivos Visualizador de Imagens Nenhum nome traduzido foi encontrado no ícone de atalho.
 Será utilizado o nome original do programa aplicativo. Definir os Aplicativos Padrão Emulador de Terminal Editor de Texto Reprodutor de Vídeo Navegador de Internet 