��          t      �                      +     8  /   E     u     �     �     �     �  �  �     a  $   o     �     �  5   �  %   �  	        )     ;     J               	                 
                            Audio Player Email Client File Manager Image Viewer No localized name found, using the original one Set Default Applications Terminal Text Editor Video Player Web Browser Project-Id-Version: desktop-defaults-set
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:13+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (https://app.transifex.com/anticapitalista/teams/10162/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Lecteur audio Logiciel de messagerie, courrielleur Gestionnaire de fichiers Visionneuse d’image Absence de nom localisé, utilisation du nom original Définir les applications par défaut Terminal  Éditeur de texte Lecteur vidéo Navigateur  