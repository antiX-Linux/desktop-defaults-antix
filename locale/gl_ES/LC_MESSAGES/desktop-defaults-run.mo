��          4      L       `   %   a   X   �   �  �   4   p  W   �                    Need to specify an application to run The program being started is myself. \n You will need to select a program other than me. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:12+0000
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2021
Language-Team: Galician (Spain) (https://www.transifex.com/anticapitalista/teams/10162/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 É necesario especificar unha aplicación a executar Este é o proprio programa a ser iniciado. \ n Deberá ser seleccionado outro programa. 