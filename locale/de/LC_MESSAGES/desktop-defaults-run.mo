��          4      L       `   %   a   X   �   d  �   8   E  r   ~                    Need to specify an application to run The program being started is myself. \n You will need to select a program other than me. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:12+0000
Last-Translator: Robin, 2021
Language-Team: German (https://www.transifex.com/anticapitalista/teams/10162/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Es ist erforderlich, ein Programm zum Starten anzugeben. Das Programm, das Sie gestartet haben, war\nder Programmstarter selbst. Sie müssen\nein anderes Programm angeben. 