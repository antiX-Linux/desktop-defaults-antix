��          t      �                      +     8  /   E     u     �     �     �     �  �  �     p     ~     �     �  8   �     �  	             *     8               	                 
                            Audio Player Email Client File Manager Image Viewer No localized name found, using the original one Set Default Applications Terminal Text Editor Video Player Web Browser Project-Id-Version: desktop-defaults-set
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:13+0000
Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2022
Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Lettore audio Client e-mail Gestore di file Visualizzatore di immagini Nessun nome localizzato trovato, in uso quello originale Imposta applicazioni di default Terminale Editor di testo Lettore video Web Browser 