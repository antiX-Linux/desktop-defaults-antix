��          t      �                      +     8  /   E     u     �     �     �     �  �  �     �     �     �     �  8   �       	   /     9     F     Y               	                 
                            Audio Player Email Client File Manager Image Viewer No localized name found, using the original one Set Default Applications Terminal Text Editor Video Player Web Browser Project-Id-Version: desktop-defaults-set
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:13+0000
Last-Translator: joyinko <joyinko@azet.sk>, 2023
Language-Team: Czech (https://www.transifex.com/anticapitalista/teams/10162/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 Přehrávač zvuků Emailový klient Správce souborů Prohlížeč obrázků Lokalizace nenalezena, použije se původní konfugurace Nastavit předvolené aplikace Terminál Editor Textu Přehrávač videa Prohlížeč 