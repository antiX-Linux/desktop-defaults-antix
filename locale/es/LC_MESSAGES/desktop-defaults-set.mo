��          t      �                      +     8  /   E     u     �     �     �     �  �  �     f     {     �     �  8   �  '   �     !     *     :     P               	                 
                            Audio Player Email Client File Manager Image Viewer No localized name found, using the original one Set Default Applications Terminal Text Editor Video Player Web Browser Project-Id-Version: desktop-defaults-set
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:13+0000
Last-Translator: Manuel <senpai99@hotmail.com>, 2022
Language-Team: Spanish (https://www.transifex.com/anticapitalista/teams/10162/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Reproductor de audio Cliente de correo electrónico Gestor de archivos Visor de imágenes No se encontró un nombre localizado, usando el original Establecer Aplicaciones Predeterminadas Terminal Editor de texto Reproductor de vídeo Buscador Web 