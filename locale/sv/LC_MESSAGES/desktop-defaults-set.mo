��          t      �                      +     8  /   E     u     �     �     �     �  �  �     F     S     `  
   m  9   x     �     �     �     �     �               	                 
                            Audio Player Email Client File Manager Image Viewer No localized name found, using the original one Set Default Applications Terminal Text Editor Video Player Web Browser Project-Id-Version: desktop-defaults-set
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:13+0000
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2022
Language-Team: Swedish (https://www.transifex.com/anticapitalista/teams/10162/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 Musikspelare E-postklient Filhanterare Bildvisare Inget lokaliserat namn hittat, använder det ursprungliga Ställ in Standard-program Terminal Textredigerare Videospelare Webbläsare 