#!/usr/bin/env python3
import re
import configparser


ConfigFileUser = 'mimeapps.list'
ConfigFileGBL = 'mimeinfo.cache'
config = configparser.ConfigParser()
mimearray = []
#print(config.sections())

#Builds Array of mime types
for line in open(ConfigFileGBL, "r"):
    try: 
        split = re.split('=', line)
        mime = split[0]
        mimearray.append(mime)
    except:
        print('none')


#Searches for an application for a mime type under defaults
def search(item):
    try:
        config.read(ConfigFileUser)
        config['Default Applications'][item]
        return(item+" | User: "+config['Default Applications'][item])
    except:
        try:
            config.read(ConfigFileGBL)
            config['MIME Cache'][item]
            return(item+" | System: "+config['MIME Cache'][item])
        except:
            return('None')
            
for mime in mimearray:
    print(search(mime))

#Adds an application for a mime type under defaults
#config['Default Applications']['text/x-editor'] = 'leafpad.desktop'
#with open(ConfigFileUser, 'w') as configfile:
#  config.write(configfile)

