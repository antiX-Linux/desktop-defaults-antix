# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# marcelocripe <marcelocripe@gmail.com>, 2022.
# 
# Translators:
# heskjestad <cato@heskjestad.xyz>, 2022
# Kristian B <mopardud@hotmail.com>, 2022
# anticapitalista <anticapitalista@riseup.net>, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: desktop-defaults-set\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-17 21:16-0300\n"
"PO-Revision-Date: 2021-04-09 14:13+0000\n"
"Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2022\n"
"Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nb\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Terminal Emulator Categories
#: desktop-defaults-set:198
msgid "Terminal"
msgstr "Terminal"

#. Web Browser Categories
#: desktop-defaults-set:199
msgid "Web Browser"
msgstr "Nettleser"

#. File Manager Categories
#: desktop-defaults-set:200
msgid "File Manager"
msgstr "Filbehandler"

#. Email Client Categories
#: desktop-defaults-set:201
msgid "Email Client"
msgstr "E-postklient"

#. Text Editor Categories
#: desktop-defaults-set:202
msgid "Text Editor"
msgstr "Tekstredigering"

#. Image Viewer Categories
#: desktop-defaults-set:203
msgid "Image Viewer"
msgstr "Bildevisning"

#. Video Player Categories
#: desktop-defaults-set:204
msgid "Video Player"
msgstr "Videoavspiller"

#. Audio Player Categories
#: desktop-defaults-set:205
msgid "Audio Player"
msgstr "Lydavspiller"

#. Window title bar text
#: desktop-defaults-set:216
msgid "Set Default Applications"
msgstr "Velg standardprogrammer"

#. Message that is displayed when the entry "Name[xx_YY]"
#. containing the language and country identifier or the
#. language identifier "Name[xx]" is not found in the
#. .desktop file. For example: "Name[pt_BR]" for Brazilian
#. Portuguese, "Name[pt]" for European Portuguese,
#. "Name[fr_BE]" for Belgian French.
#: desktop-defaults-set:243
msgid "No localized name found, using the original one"
msgstr "Fant ikke lokaltilpasset navn, bruker det originale"
