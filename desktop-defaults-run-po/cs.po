# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# joyinko <joyinko@azet.sk>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-09 17:08+0300\n"
"PO-Revision-Date: 2021-04-09 14:12+0000\n"
"Last-Translator: joyinko <joyinko@azet.sk>, 2021\n"
"Language-Team: Czech (https://www.transifex.com/anticapitalista/teams/10162/cs/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: cs\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#: desktop-defaults-run:60
msgid "Need to specify an application to run"
msgstr "Je potřebné vybrat aplikaci ke spuštění"

#: desktop-defaults-run:81
msgid ""
"The program being started is myself. \\n You will need to select a program "
"other than me."
msgstr ""
"Spuštěn stejný program. \\n Je zapotřebí vybrat jiný program jako je ten "
"špuštěný."
