# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-09 17:09+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: desktop-defaults-set:31
msgid "Terminal:FL"
msgstr ""

#: desktop-defaults-set:32
msgid "Web Browser:FL"
msgstr ""

#: desktop-defaults-set:33
msgid "File Manager:FL"
msgstr ""

#: desktop-defaults-set:34
msgid "Email Client:FL"
msgstr ""

#: desktop-defaults-set:35
msgid "Text Editor:FL"
msgstr ""

#: desktop-defaults-set:36
msgid "Image Viewer:FL"
msgstr ""

#: desktop-defaults-set:37
msgid "Video Player:FL"
msgstr ""

#: desktop-defaults-set:38
msgid "Audio Player:FL"
msgstr ""

#: desktop-defaults-set:39
msgid "Set Default Applications"
msgstr ""
